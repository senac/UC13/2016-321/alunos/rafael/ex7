/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.megaxhost.ex7;

import java.util.Scanner;

/**
 *
 * @author MegaxHost
 */
public class PasseiNaUc13 {

 public static void main(String[] args) {
        float nota; 
        Scanner entrada = new Scanner(System.in);
     
        System.out.print("Digite sua nota [0.0 - 10.0]: " );
        nota = entrada.nextFloat();
     
        if( (nota <= 10.0) && (nota >= 0.0) ){
         
            if( nota >= 7.0 ){
                System.out.println("Parabéns, você passou na Uc13");
            }
            else {
             
                if ((nota >= 4.0) && (nota <= 6.0)) {
                     System.out.println("Bom deu ruim para você, Vai ter que fazer recuperação para ser aprovado na Uc13");
                }
                else {
                     System.out.println("Reprovado. mais uma vez volte na proxima turma.");
                }
            }
         
        }
        else {
            System.out.println("Nota inválida, fechando aplicativo");
        }
         
     
    }
}