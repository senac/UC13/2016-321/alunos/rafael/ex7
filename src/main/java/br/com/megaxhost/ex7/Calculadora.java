/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.megaxhost.ex7;

/**
 *
 * @author sala304b
 */
public class Calculadora {
   public static final String APROVADO = "Aprovado";
    public static final String RECUPERACAO = "Recuperacao";
    public static final String REPROVADO = "Reprovado";
    
    public String calcular (double nota){
        
        if(nota >= 7){
            return APROVADO;
        }
        if(nota < 7 && nota >=4){
            return RECUPERACAO;
        }
        
        return REPROVADO;
    }    
}
